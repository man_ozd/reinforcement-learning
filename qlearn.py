import random
import numpy as np
import gym
from gym_omni.envs import OmniEnv
from gym_omni.simple_world import World, Agent
import numpy as np
import random
from progressbar import progressbar
import csv
from nn import neural_net, LossHistory
import os.path
import timeit


WORLD_SIZE = (10, 10)
ROBOT_SIZE = 0.28
LASERS_N = 24
START_XY = (0, 0)
GOAL_XY = (3, 0)

NUM_INPUT = LASERS_N + 2
GAMMA = 0.9  # Forgetting.
TUNING = False  # If False, just use arbitrary, pre-selected params.

OBSERVE = 1000
TRAIN_FRAMES = 30000
SAVE_EVERY_N = 5
EPS = 1

NN_PARAM = [164, 150]
BATCH_SIZE = 400
BUFFER = 50000


angle = int(360 / LASERS_N)
view_angles = range(0, 360, angle)
world = World(WORLD_SIZE)
agent = Agent(world, START_XY, size=ROBOT_SIZE, view_angles=view_angles)
env = gym.make("omni-v0", agent=agent, goal_xy=GOAL_XY)


def train_net(model, params):

    filename = params_to_filename(params)

    epsilon = EPS
    batchSize = params["batchSize"]
    buffer = params["buffer"]

    # Just stuff used below.
    t = 0
    data_collect = []
    replay = []  # stores tuples of (S, A, R, S').

    loss_log = []

    # Create a new game instance.
    state = env.state()

    # Let's time it.
    start_time = timeit.default_timer()
    action = None
    # Run the frames.
    for t in progressbar(range(TRAIN_FRAMES + 1)):
        data_collect.append(
            [
                t,
                action,
                env.state()[0][0],
                env.state()[0][1],
                env.state()[1][0],
                env.state()[1][1],
            ]
        )
        # Choose an action.
        if random.random() < epsilon or t < OBSERVE:
            action = np.random.choice(range(len(view_angles)))  # random
        else:
            state = np.array([state])
            # Get Q values for each action.
            qval = model.predict(state, batch_size=1)
            action = np.argmax(qval)  # best

        # Take action, observe new state and get our treat.
        new_state, reward, done = env.step(action)
        # Experience replay storage.
        replay.append((state, action, reward, new_state))

        # If we're done observing, start training.
        if t > OBSERVE:

            # If we've stored enough in our buffer, pop the oldest.
            if len(replay) > buffer:
                replay.pop(0)

            # Randomly sample our experience replay memory
            minibatch = random.sample(replay, batchSize)

            # Get training values.
            X_train, y_train = process_minibatch2(minibatch, model)

            # Train the model on this batch.
            history = LossHistory()
            model.fit(
                X_train,
                y_train,
                batch_size=batchSize,
                nb_epoch=1,
                verbose=0,
                callbacks=[history],
            )
            loss_log.append(history.losses)

        # Update the starting state with S'.
        state = new_state

        # Decrement epsilon over time.
        if epsilon > 0.1 and t > OBSERVE:
            epsilon -= 1.0 / TRAIN_FRAMES

        # We died, so update stuff.
        if reward <= -500:
            # Log the car's distance at this T.
            # data_collect.append([t, env.state()])

            # Update max.
            # Time it.
            tot_time = timeit.default_timer() - start_time
            fps = t / tot_time
            # Output some stuff so we can watch.
            # print("Max: %d at %d\tepsilon %f\t(%d)\t%f fps" %
            #       (max_car_distance, t, epsilon, car_distance, fps))

            # Reset.
            env.reset()
            state = env.state()
            start_time = timeit.default_timer()

        # Save the model every 25,000 frames.
        if t % SAVE_EVERY_N == 0:
            model.save_weights(
                "saved-models/" + filename + "-" + str(t) + ".h5", overwrite=True
            )
            print("Saving model %s - %d" % (filename, t))
            log_results(filename, data_collect, loss_log)

        if done:
            env.reset()
            state = env.state()
            start_time = timeit.default_timer()
    # Log results after we're done all frames.
    log_results(filename, data_collect, loss_log)


def log_results(filename, data_collect, loss_log):
    # Save the results to a file so we can graph it later.
    with open("results/learn_data-" + filename + ".csv", "w") as data_dump:
        wr = csv.writer(data_dump)
        wr.writerows(data_collect)

    with open("results/loss_data-" + filename + ".csv", "w") as lf:
        wr = csv.writer(lf)
        for loss_item in loss_log:
            wr.writerow(loss_item)


def process_minibatch2(minibatch, model):
    # by Microos, improve this batch processing function
    #   and gain 50~60x faster speed (tested on GTX 1080)
    #   significantly increase the training FPS

    # instead of feeding data to the model one by one,
    #   feed the whole batch is much more efficient

    mb_len = len(minibatch)

    old_states = np.zeros(shape=(mb_len, 26))
    actions = np.zeros(shape=(mb_len,))
    rewards = np.zeros(shape=(mb_len,))
    new_states = np.zeros(shape=(mb_len, 26))
    # print(minibatch)
    for i, m in enumerate(minibatch):
        old_state_m, action_m, reward_m, new_state_m = m
        old_states[i, :] = old_state_m[...]
        actions[i] = action_m
        rewards[i] = reward_m
        new_states[i, :] = new_state_m[...]

    old_qvals = model.predict(old_states, batch_size=mb_len)
    new_qvals = model.predict(new_states, batch_size=mb_len)

    maxQs = np.max(new_qvals, axis=1)
    y = old_qvals

    non_term_inds = np.where(rewards >= -500)[0]
    # print(non_term_inds)
    # print(actions[non_term_inds].astype(int))
    # print(rewards[non_term_inds] + (GAMMA * maxQs[non_term_inds]))
    # print(y)
    term_inds = np.where(rewards <= -500)[0]

    y[non_term_inds, actions[non_term_inds].astype(int)] = rewards[non_term_inds] + (
        GAMMA * maxQs[non_term_inds]
    )
    y[term_inds, actions[term_inds].astype(int)] = rewards[term_inds]

    X_train = old_states
    y_train = y
    return X_train, y_train


def params_to_filename(params):
    return (
        str(params["nn"][0])
        + "-"
        + str(params["nn"][1])
        + "-"
        + str(params["batchSize"])
        + "-"
        + str(params["buffer"])
    )


def launch_learn(params):
    filename = params_to_filename(params)
    print("Trying %s" % filename)
    # Make sure we haven't run this one.
    if not os.path.isfile("results/sonar-frames/loss_data-" + filename + ".csv"):
        # Create file so we don't double test when we run multiple
        # instances of the script at the same time.
        open("results/sonar-frames/loss_data-" + filename + ".csv", "a").close()
        print("Starting test.")
        # Train.
        model = neural_net(NUM_INPUT, params["nn"])
        train_net(model, params)
    else:
        print("Already tested.")


if __name__ == "__main__":
    if TUNING:
        param_list = []
        nn_params = [[164, 150], [256, 256], [512, 512], [1000, 1000]]
        batchSizes = [40, 100, 400]
        buffers = [10000, 50000]

        for nn_param in nn_params:
            for batchSize in batchSizes:
                for buffer in buffers:
                    params = {"batchSize": batchSize, "buffer": buffer, "nn": nn_param}
                    param_list.append(params)

        for param_set in param_list:
            launch_learn(param_set)

    else:
        params = {"batchSize": BATCH_SIZE, "buffer": BUFFER, "nn": NN_PARAM}
        model = neural_net(NUM_INPUT, NN_PARAM)
        train_net(model, params)
