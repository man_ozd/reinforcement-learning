import os
import csv
import timeit
import random
import logging

import gym
import hydra
import numpy as np
import progressbar
from omegaconf import DictConfig

from gym_omni.envs import OmniEnv
from utils import log_results
from process import fit_model, process_minibatch2
from gym_omni.simple_world import World, Agent
from keras_models.model import get_model, LossHistory

FORMAT = "%(levelname)s %(asctime)s %(module)s:%(lineno)d] %(message)s"
logging.basicConfig(format=FORMAT, level=logging.INFO)
logger = logging.getLogger(__name__)


@hydra.main(config_path="../conf/config.yaml")
def main(cfg: DictConfig) -> None:
    assert (
        cfg.train.replay_memory > cfg.train_batch_size
    ), "Wrong replay memory of batch size"

    view_angles = range(0, 360, cfg.agent.view_angle)
    move_angles = range(0, 360, cfg.agent.move_angle)
    states_size = len(view_angles) + 4
    model = get_model(states_size, cfg.train.nn_param, len(move_angles))
    world = World(tuple(cfg.world.size), n_obstacles=cfg.world.n_obstacles)
    agent = Agent(
        world,
        tuple(cfg.agent.start_xy),
        tuple(cfg.agent.goal_xy),
        size=cfg.agent.size,
        speed=cfg.agent.speed,
        move_angles=move_angles,
        view_angles=view_angles,
    )
    env = gym.make("omni-v0", agent=agent)
    action_space = range(int(360 / cfg.agent.move_angle))
    # print(action_space)
    replay = []
    data_collect = []
    loss_log = []
    total_steps = 0
    epsilon = cfg.train.epsilon

    bar = progressbar.ProgressBar(maxval=cfg.train.max_steps).start()
    for i in range(cfg.train.episodes):
        if total_steps >= cfg.train.max_steps:
            break
        env.reset()
        ob = env.state()
        # start_time = timeit.default_timer()
        done = False
        action = None
        ep_steps = 0

        while done is False:
            action = agent.act(ob, reward, done)
            new_ob, reward, done = env.step(action)
            replay.append((ob, action, reward, new_ob))

            # use_replay_to_fit_model
            if len(replay) > cfg.train.replay_memory:
                train_model(replay)
            #     replay.pop(0)
            #     model, loss_log = fit_model(
            #         model,
            #         replay,
            #         cfg.train.batch_size,
            #         loss_log,
            #         states_size,
            #         gamma=cfg.train.gamma,
            #     )

            # # Update the starting state with S'.
            # state = new_state

            # ep_steps += 1
            # total_steps += 1
            # bar.update(total_steps)
            # epsilon -= 1 / cfg.train.max_steps

            # if total_steps % cfg.model.save_per_steps == 0:
            #     model_dir = os.path.join(cfg.model.dir, f"{cfg.id.count}")
            #     os.makedirs(model_dir, exist_ok=True)
            #     path = os.path.join(
            #         model_dir, f"model_{cfg.id.count}_ep{i}_{total_steps}.h5"
            #     )
            #     model.save_weights(path, overwrite=True)
            #     results_dir = os.path.join(cfg.train.results_dir, f"{cfg.id.count}")
            #     os.makedirs(results_dir, exist_ok=True)
            #     filename = f"{cfg.id.count}_{total_steps}"
            #     log_results(results_dir, filename, data_collect, loss_log)
            ob = new_ob
            if done:
                break

    bar.finish()
    env.close()


if __name__ == "__main__":
    main()
